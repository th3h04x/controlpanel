import { Controller, Post } from '@nestjs/common';
import { ResponsesService } from './responses.service';

@Controller('responses')
export class ResponsesController {

    constructor(
        private readonly responsesController : ResponsesService
    ){}

    @Post('/getAllRatings')
    async getAllRatings(){

    }


    @Post('/getAllFeedbacks')
    async getAllFeedbacks(){

    }

    @Post('/getAllIssues')
    async getAllIssues(){

    }

    @Post('/updateReviews')
    async updateReviews(){

    }

    @Post('getAllBookings')
    async getAllBookings(){

    }

    @Post('bookCharger')
    async bookCharger(){

    }

    @Post('/updateBookings')
    async updateBookings(){

    }

}
