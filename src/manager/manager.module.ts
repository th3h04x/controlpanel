import { Module } from '@nestjs/common';
import { ManagerService } from './manager.service';
import { ManagerController } from './manager.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Manager} from 'src/models/manager/Manager.models';
import * as moment from 'moment'

@Module({
  providers: [ManagerService],
  controllers: [ManagerController]
})


export class ManagerModule {}
