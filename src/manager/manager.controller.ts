import { Controller, Get, Post } from '@nestjs/common';
import { ManagerService } from './manager.service';

@Controller('manager')
export class ManagerController {
    constructor(
        private readonly managerService : ManagerService
    ){}
    
    @Post('/createManager')
    async createManager(){
        return {
            hello : "hello"
        }
    }

    @Get('/allManagerDetails')
    async allManagerDetails(){

    }

    @Get('/unasignedManagers')
    async unassignedManagers(){

    }

    @Post('/managerDetails')
    async managerDetails(){

    }

    @Get('/totalWithdrawalRequests')
    async totalWithdrawalRequests(){

    }

    @Post('/assignCharger')
    async assignCharger(){

    }

    @Post('/gtv')
    async gtv(){

    }

    @Post('/chargers')
    async chargers(){

    }

    @Post('/managerCurrentBalance')
    async managerCurrentBalance(){

    }

    @Get('/totalCurrentBalance')
    async totalCurrentBalance(){

    }

    @Get('/requestedAmounts')
    async requestedAmounts(){

    }

    @Get('/getAllRequests')
    async getAllRequests(){

    }

}
