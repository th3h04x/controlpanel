import {HttpException, HttpStatus, Injectable , NestMiddleware} from "@nestjs/common"
import { InjectModel } from "@nestjs/mongoose"
import * as jwt from 'jsonwebtoken'
import { Model } from "mongoose"
import { Manager } from "src/models/manager/Manager.models"

@Injectable()
export class JwtAuthMiddleware implements NestMiddleware{

    constructor(
        @InjectModel('Manager') private readonly managerModel: Model<Manager>
    ){}

    async use(req : Request , res : Response , next: ()=> void){

        const jwtToken = req.headers["authorization"]
        if(!jwtToken){
            throw new HttpException("failed in token verification",HttpStatus.UNAUTHORIZED)
        }
        try{
            let manager = jwt.verify(jwtToken,process.env.JWT_SECRET)
            if(manager){
                // verify jwt token belongs to admin
                console.log(manager)

                const managerDetails = await this.managerModel.findById(manager["id"])

                if(managerDetails.role === "admin"){
                    // token verification succeded
                    req["user"] = managerDetails._id
                    next()
                }
                else{
                    throw new HttpException("forbidden",HttpStatus.FORBIDDEN)
                }

            }
            else{
                throw new HttpException("failed in token verification",HttpStatus.UNAUTHORIZED)
            }
        }
        catch(err){
            console.log("failed in jwt middleware : " , err)
            throw new HttpException("failed in token verification",HttpStatus.UNAUTHORIZED)
        }

        next()
    }

}