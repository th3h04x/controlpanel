import { MiddlewareConsumer, Module , NestModule} from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {ConfigModule , ConfigService} from '@nestjs/config'
import { MongooseModule } from '@nestjs/mongoose';
import { JwtAuthMiddleware } from 'src/middlewares/managerAuthMiddleware';
import { HomeModule } from './home/home.module';
import { ChargerModule } from './charger/charger.module';
import { ManagerModule } from './manager/manager.module';
import { ResponsesModule } from './responses/responses.module';
import { FinanceModule } from './finance/finance.module';
import { UsersModule } from './users/users.module';
@Module({
  imports: [
    ConfigModule.forRoot({isGlobal : true}),
    MongooseModule.forRoot(
      `mongodb+srv://${process.env.MONGODB_USERNAME}:${process.env.MONGODB_PASSWORD}@energymeterdata.9vrix.mongodb.net/test_veChargePoint?retryWrites=true&w=majority`
    ),
    HomeModule,
    ChargerModule,
    ManagerModule,
    ResponsesModule,
    FinanceModule,
    UsersModule
  ],
  controllers: [AppController],
  providers: [AppService],
})

export class AppModule{}
