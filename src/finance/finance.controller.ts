import { Controller, Post } from '@nestjs/common';
import { FinanceService } from './finance.service';

@Controller('finance')
export class FinanceController {
    constructor(
        private readonly financeService : FinanceService
    ){}

    @Post('/processPayment')
    async processPayment(){

    }

    @Post('/changeCommision')
    async changeCommision(){

    }
    
}
