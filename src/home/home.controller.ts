import { Controller, Post } from '@nestjs/common';
import { HomeService } from './home.service';

@Controller('home')
export class HomeController {
    constructor(
        private readonly homeService : HomeService
    ){}

    @Post('/getTotalInfo')
    async getTotalInfo(){
        return {
            
        }
    }

    @Post('/getLiveChargers')
    async getLiveChargers(){

    }

    @Post('getChargerHistory')
    async getChargerHistory(){

    }

}
