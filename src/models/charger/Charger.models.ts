import { Prop , Schema } from '@nestjs/mongoose';
import moment from 'moment';
import {Date, Document} from 'mongoose'
import * as mongoose from 'mongoose'
import { Manager } from 'src/models/manager/Manager.models';
import { User } from 'src/models/user/User.models';

@Schema()
export class Charger extends Document{
    @Prop({
        type: String,
        required: [true, "certificate is a required field"],
        select: false
    })
    certificate : string

    @Prop({
        type: String,
        required: [true, "Private Key is a required field"],
        select: false
    })
    privateKey : string

    @Prop({
        type: String,
        required: [true, "Charger Name is a required field"],
        // unique: [true, "Charger with this name already exists"]
    })
    _id : string

    @Prop({
        type: String,
        default: "OFF",
        enum: ["ON", "OFF"]
    })
    status : string

    @Prop({
        type:Boolean
    })
    wifiConnectivity : boolean

    @Prop()
    location : any

    @Prop({
        type: Number,
        default: 0,
        set: val => Math.round(val * 100)/100
    })
    power : number

    @Prop({
        type: Number,
        default: 0,
        set: val => Math.round(val * 100)/100
    })
    voltage : number

    @Prop( {
        type: Number,
        default: 0,
        set: val => Math.round(val * 100)/100
    })
    current : number

    @Prop({
        type: Number,
        default: 0,
        set: val => Math.round(val * 100)/100
    })
    energy : number

    @Prop( {
        type: Date
    })
    connectedTimeStamp : any

    @Prop({
        type : mongoose.Schema.Types.ObjectId ,
        ref : 'User',
        default : null
    })
    currentUser : User

    @Prop({
        type : mongoose.Schema.Types.ObjectId ,
        ref : 'Manager'
    })
    managerId : Manager

    @Prop({
        type: Number,
        required: [true, "regular price is required attribute"],
        select: false
    })
    regularPrice : number

    @Prop({
        type: Number,
        required: [true, "special price is required attribute"],
        select: false
    })
    specialPrice : number

    @Prop({
         type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }] ,
         default : [],
         select : false
        })
    specialUsers : User[]

    @Prop({
        type: Boolean,
        default: false
    })
    isActive : boolean

    @Prop({
        type : String
    })
    detail : string

    @Prop({
        type : String
    })
    operator : string

    @Prop({
        type : String
    })
    address : string

    @Prop({
        type : String
    })
    number : string

    @Prop({
        type : String
    })
    email : string

}

