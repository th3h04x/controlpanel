import { SchemaFactory } from "@nestjs/mongoose";
import { SocketConnection } from "./SocketConnection.model";

export const SocketConnectionSchema = SchemaFactory.createForClass(SocketConnection)

SocketConnectionSchema.index({userId: 1})

