import * as mongoose from 'mongoose'
import * as moment from 'moment'
import {Date, Document} from 'mongoose'
import {Prop, Schema} from '@nestjs/mongoose'
import { User } from '../user/User.models'
import { Manager } from '../manager/Manager.models'

@Schema()
export class Finance extends Document{
    @Prop({
        type: String,
        required: [true, "Account Number is required!"]
    })
    accountNumber : string

    @Prop({
        type: String,
        required: [true, "IFSC Code is required"]
    })
    IFSC : string

    @Prop({
        type: Number,
        required: [true, "Amount is required!"],
        set: val => Math.round(val * 100)/100
    })
    amount : number

    @Prop({
        type: String,
        default: "not processed",
        enum: ["not processesed", "processesed", "paid"]
        // pending , accepted , not accepted
    })
    status : string

    @Prop({
        type: String
    })
    acknowledgementMessage : string

    @Prop({
        type: mongoose.Schema.Types.ObjectId,
        ref: "managers",
        required: [true, "Manager Id is required!"]
    })
    managerId : Manager

    @Prop({
        type: Date,
        default: moment().toDate()
    })
    createdAt : any
}