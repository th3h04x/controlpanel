import { SchemaFactory } from "@nestjs/mongoose";
import { Rating } from "./Rating.model";
import * as moment from 'moment'

export const RatingSchema = SchemaFactory.createForClass(Rating)

RatingSchema.pre<Rating>('save',function(next : Function){
    if(this.isNew){
        this.createdAt = moment().toDate()
    }
    next()
})