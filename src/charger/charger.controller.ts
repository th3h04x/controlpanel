import { Controller, Get, Post } from '@nestjs/common';
import { ChargerService } from './charger.service';

@Controller('charger')
export class ChargerController {
    constructor(
        private readonly chargerService : ChargerService
    ){}

    @Post('/create')
    async create(){

    }

    @Get('/unassigned')
    async unassigned(){

    }

    @Post('/allDetails')
    async allDetails(){

    }

    @Post('/activate')
    async activate(){

    }

    @Post('/deactivate')
    async deactivate(){

    }
    
}
