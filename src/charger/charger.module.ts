import { Module } from '@nestjs/common';
import { ChargerService } from './charger.service';
import { ChargerController } from './charger.controller';

@Module({
  providers: [ChargerService],
  controllers: [ChargerController]
})
export class ChargerModule {}
